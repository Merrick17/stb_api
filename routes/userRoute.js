const express = require('express'); 
const conn = require('../config/DbConn'); 
const bcrypt=require('bcryptjs'); 
const jwt = require('jsonwebtoken'); 
const config = require('../config/config'); 
const verifToken = require('../config/verifToken'); 
const saltRound = 10; 
const router = express.Router(); 


router.post('/signup',(req,res,next)=>{
    //console.log(req.body);
const nom = req.body.nom ; 
const prenom = req.body.prenom ; 
const date_naiss=req.body.date_naiss ; 
const email = req.body.email ; 
const pass = req.body.mdp ; 
const type = req.body.type ; 

const sql1="SELECT id_user FROM Users WHERE email= ? " ; 
conn.query(sql1,[email],(err,result,fields)=>{
    console.log(result); 
if(err) throw err; 
if(result.length!=0)
{
    res.json({"err":"email exist"}); 
    
}else
{ const sql ="INSERT INTO `users`(`nom`, `prenom`, `datenaiss`, `email`, `mdp`, `type`) VALUES (?,?,?,?,?,?)";
const hash=bcrypt.hashSync(pass,saltRound); 
conn.query(sql,[nom,prenom,date_naiss,email,hash,type],(err,result,fields)=>{
    if(err)
    { 
        res.json({"err":err})
    }else
    {   const token = jwt.sign({id:result.insertId},config.secret,{
        expiresIn:86400
    }); 
        res.json({"UserID":result.insertId, 
                    "auth":true,
                    "token":token  
                }); 
    }
}); 

}

});

}); 
    




router.post('/login',(req,res,next)=>{
const email=req.body.email ; 
const psw =req.body.password ; 

const sql="SELECT * FROM Users WHERE email =? " ; 
conn.query(sql,[email],(err,result,fields)=>{
if(err) throw err; 
if(result.length!=0)
{
    const mdp =result[0].mdp;
    const comp=bcrypt.compareSync(psw,mdp); 
    console.log(comp);
  
    if(comp==true)
    { 
        const token = jwt.sign({id:result.id_user},config.secret,{
            expiresIn:86400
        }); 
            res.json({"UserID":result[0].id_user, 
                        "auth":true,
                        "token":token ,
                        "role":result[0].type 
                    }); 


    }else
    { console.log("mdp:"+mdp); 
        console.log("password"+psw); 
        res.json({"msg":"mot de passe ou adresse incorrecte"}); 
    }


  

   
}else
{

    res.json({"msg":"mot de passe ou adresse incorrecte"}); 
}

});

}); 
    


router.get('/:id',verifToken,(req,res,next)=>{
   
   const id= req.params.id;
    const sql="SELECT * FROM Users WHERE id_user = ? " ; 
    conn.query(sql,[id],(err,result,fields)=>{
    if(err) throw err; 
    if(result.length!=0)
    {
       res.json({"result":result})
    
      
    
       
    }else
    {
    
        res.json({"msg":"mot de passe ou adresse incorrecte"}); 
    }
    
    });
    
    }); 
    router.get('/',verifToken,(req,res,next)=>{
   
    
        const sql="SELECT * FROM users  " ; 
        conn.query(sql,[],(err,result,fields)=>{
        if(err) throw err; 
        if(result.length!=0)
        {
           res.json({"result":result})
        
          
        
           
        }else
        {
        
            res.json({"msg":"mot de passe ou adresse incorrecte"}); 
        }
        
        });
        
        }); 
    

    router.put('/update/:id',verifToken,(req,res,next)=>{
          const id = req.params.id; 
          const nom = req.body.nom ; 
          const prenom = req.body.prenom ; 
          const date_naiss=req.body.date_naiss ; 
          const email = req.body.email ; 
          const pass = req.body.mdp ;  
          const hash=bcrypt.hashSync(pass,saltRound); 
         const sql="UPDATE `users` SET `nom`=?,`prenom`=?,`datenaiss`=?,`email`=?,`mdp`=? WHERE user_id = ?" ; 
        conn.query(sql,[nom,prenom,date_naiss,email,hash,id],(err,result,fields)=>{
        if(err) throw err; 
        if(result.length!=0)
        {
           res.json({"result":result})
        
          
        
           
        }else
        {
        
            res.json({"msg":"mot de passe ou adresse incorrecte"}); 
        }
        
        });
        
        }); 
            
        router.put('/prenom/:id',verifToken,(req,res,next)=>{
   
            const id = req.params.id; 
            const prenom = req.params.prenom ;
            const sql="UPDATE `users` SET `prenom`=? WHERE user_id = ? " ; 
            conn.query(sql,[id,prenom],(err,result,fields)=>{
            if(err) throw err; 
            if(result.length!=0)
            {
               res.json({"result":result})
            
              
            
               
            }else
            {
            
                res.json({"msg":"mot de passe ou adresse incorrecte"}); 
            }
            
            });
            
            });       
        
            router.delete('/:id',verifToken,(req,res,next)=>{
   
                const id = req.params.id; 
                //const prenom = req.params.prenom ;
                const sql="DELETE FROM `users` WHERE id_user = ?" ; 
                conn.query(sql,[id,prenom],(err,result,fields)=>{
                if(err) throw err; 
                if(result.length!=0)
                {
                   res.json({"result":result})
                
                  
                
                   
                }else
                {
                
                    res.json({"msg":"mot de passe ou adresse incorrecte"}); 
                }
                
                });
                
                });       
            



module.exports=router ; 