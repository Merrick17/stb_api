const mysql =require('mysql');
const express = require('express');
const cors = require('cors'); 
const conn = require('./config/DbConn'); 
var app = express();
app.use(cors());
const bodyparser = require('body-parser');
const userRoute = require('./routes/userRoute'); 
const creditRoute = require('./routes/demandeCredit'); 
const paperRoute = require('./routes/papersRoute'); 
app.use(bodyparser.urlencoded({extended:false}));
app.use(bodyparser.json()); 

app.use('/users',userRoute); 
app.use('/credit',creditRoute); 
app.use('/papers',paperRoute); 





app.listen(3000,()=>console.log('Express server is running at port no : 3000'));
